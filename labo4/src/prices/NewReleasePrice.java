package prices;

public class NewReleasePrice extends Price {

	public double amount(int daysRented) {

		return daysRented * 3;
	}

	@Override
	public int points() {

		return 2;
	}

}
