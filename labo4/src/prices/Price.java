package prices;

public abstract class Price {

	public abstract double amount(int daysRented);

	public int points() {

		return 1;
	}

}
