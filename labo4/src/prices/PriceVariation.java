package prices;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PriceVariation implements Comparable<PriceVariation>{
	
	public LocalDate rentalDate;
	public Price rentalCost;
	
	public LocalDate getRentalDate() {
		return rentalDate;
	}

	public PriceVariation(LocalDate date, Price cost) {
		rentalDate = date;
		rentalCost = cost;
	}

	public Price getRentalCost() {
		return rentalCost;
	}
	
	public int compareTo(PriceVariation other) {
		return rentalDate.compareTo(other.rentalDate);
	}

}
