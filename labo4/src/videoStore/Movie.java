/*
 * Created on 2005-03-07
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package videoStore;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.Collections;

import prices.ChildrenPrice;
import prices.NewReleasePrice;
import prices.Price;
import prices.PriceVariation;
import prices.RegularPrice;
import prices.UnpopularPrice;

/**
 * M. Fowler, et al., Refactoring, Improving the design of existing code,
 * Addison-Wiley, 2000. Exemple Chapitre 1
 * 
 * The class Movie is just a simple data class
 */

public class Movie {
	public static final Price REGULAR = new RegularPrice();
	public static final Price NEW_RELEASE = new NewReleasePrice();
	public static final Price CHILDRENS = new ChildrenPrice();
	public static final Price UNPOPULAR = new UnpopularPrice();

	private String title_;
	private Price priceCode_;
	private Rental rental;
	private ArrayList<PriceVariation> PriceList;

	private Movie(String title, Price priceCode) {
		title_ = title;
		PriceList = Movie.createList();
		setPriceCode(priceCode, LocalDate.now());
	}

	public String getTitle() {
		return title_;
	}

	public void setPriceCode(Price newCode, LocalDate date) {
		priceCode_ = newCode;
		PriceVariation datePrice = new PriceVariation(date, newCode);
		PriceList.add(datePrice);
		
	}

	public Price priceCode(LocalDate dateRecherchee) {
		Collections.sort(PriceList);
		for (PriceVariation i : PriceList) {
			if(dateRecherchee.isAfter(i.rentalDate) || dateRecherchee.isEqual(i.rentalDate)) {
				priceCode_ = i.getRentalCost();
			}

		}

		return priceCode_;
	}

	public double amount(int daysRented, LocalDate rentalDate) {
		return priceCode(rentalDate).amount(daysRented);
	}

	public int points() {
		return priceCode_.points();
	}

	public static ArrayList<PriceVariation> createList() {

		ArrayList<PriceVariation> prices = new ArrayList<>();

		return prices;
	}

	public static Movie createChildrenMovie(String name) {

		return new Movie(name, Movie.CHILDRENS);
	}

	public static Movie createRegularMovie(String name) {

		return new Movie(name, Movie.REGULAR);
	}

	public static Movie createNewReleaseMovie(String name) {

		return new Movie(name, Movie.NEW_RELEASE);
	}

	public static Movie createUnpopularMovie(String name) {

		return new Movie(name, Movie.UNPOPULAR);
	}
}
