package videoStore.tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import videoStore.Customer;
import videoStore.Movie;
import videoStore.Rental;

public class CustomerTest {

	private Customer grandmaman;
	private Movie movies[];

	@Before
	public void setup() {
		movies = new Movie[5];

		movies[0] = Movie.createChildrenMovie("The Batman");
		movies[1] = Movie.createNewReleaseMovie("Fast and furious 38: Diesel Cars vs Google Cars");
		movies[2] = Movie.createNewReleaseMovie("31 Jump Street: Ninja Academy");
		movies[3] = Movie.createRegularMovie("Les oiseaux se cachent pour mourir");
		movies[4] = Movie.createRegularMovie("Les gars chauds se cachent pour vomir");

		grandmaman = new Customer("Grand-moman");

		grandmaman.addRentals(new Rental(movies[0], 4));
		grandmaman.addRentals(new Rental(movies[1], 1));
		grandmaman.addRentals(new Rental(movies[2], 2));
		grandmaman.addRentals(new Rental(movies[3], 7));
		grandmaman.addRentals(new Rental(movies[4], 1));
		
		/*Exemple de création de location avec une date passée*/
		String str = "2022-03-09";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(str, formatter);
		Rental testDate = new Rental (movies[0],date,2);
		
		
		
	}
	
	@Test
	public void testPriceVariation() {
		
		LocalDate today = LocalDate.now();
		LocalDate deuxSemaine = today.plusWeeks(2);
		LocalDate sixMonth = today.plusMonths(6);
		
		Rental test = new Rental (movies[0], today, 3);
		Rental test1 = new Rental(movies[0], deuxSemaine, 4);
		Rental test2 = new Rental(movies[0], sixMonth, 2);
		
		movies[0].setPriceCode(Movie.REGULAR, deuxSemaine);
		assertEquals(test.amount(), 1.5, 0.000001);
		assertEquals(test1.amount(), 5, 0.000001);
		assertEquals(test2.amount(), 2, 0.000001);
		
		movies[0].setPriceCode(Movie.UNPOPULAR, sixMonth);
		assertEquals(test1.amount(), 5, 0.000001);
		assertEquals(test2.amount(), 2, 0.000001);
		assertEquals(test2.amount(), 2, 0.000001);
	}

	

	@Test
	public void testTotalAmount() {

		assertEquals(grandmaman.totalAmount(), 23.5, 0.0001);
	}

	@Test
	public void testTotalPoints() {

		assertEquals(grandmaman.totalPoints(), 7);
	}

	@After
	public void teardown() {
		grandmaman = null;
		movies = null;
	}

}
